package info.hccis.tutoring.dao;

import info.hccis.tutoring.entity.Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * DAO class to get students.
 *
 * @author bjmaclean
 * @since 20211008
 * @modified fhm 20211018
 */
public class StudentDAO {

	private static ResultSet rs;
	private static Connection conn = null;

	public StudentDAO() {

		String propFileName = "application";
		ResourceBundle rb = ResourceBundle.getBundle(propFileName);
		String connectionString = rb.getString("spring.datasource.url");
		String userName = rb.getString("spring.datasource.username");
		String password = rb.getString("spring.datasource.password");

		try {
			conn = DriverManager.getConnection(connectionString, userName, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Select students by their skill type.
	 *
	 * @since 20210924
	 * @author BJM
	 * @modified fhm 20211018
	 */
	public ArrayList<Student> selectStudentsBySkillId(int id) {
		Statement stmt;
		ArrayList<Student> students = new ArrayList();
		try {
			stmt = conn.createStatement();
			String query = "select student.id, student.firstName, student.lastName, student.phoneNumber, student.emailAddress from "
					+ "student, studentskill where "
					+ "student.id = studentskill.studentId "
					+ "and studentskill.skillType = " + id + ";";
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				Student student = new Student();
				student.setId(rs.getInt("id"));
				student.setFirstName(rs.getString("firstName"));
				student.setLastName(rs.getString("lastName"));
				student.setPhoneNumber(rs.getString("phoneNumber"));
				student.setEmailAddress(rs.getString("emailAddress"));
				students.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return students;
	}

}
