package info.hccis.tutoring.bo;

import info.hccis.tutoring.dao.CodeDAO;
import info.hccis.tutoring.entity.Code;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 * @modified fhm 20211018
 */
public class CodeBO {

	public ArrayList<Code> getCode(byte code) {
		CodeDAO courseDAO = new CodeDAO();
		return courseDAO.getCode(code);
	}
}
