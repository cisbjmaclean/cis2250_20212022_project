package info.hccis.tutoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main controller.
 *
 * @author N/A
 * @since N/A
 */
@SpringBootApplication
public class TutoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutoringApplication.class, args);
	}

}
