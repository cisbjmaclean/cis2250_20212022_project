package info.hccis.tutoring.repositories;

import info.hccis.tutoring.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Simple repository for database interaction
 *
 * @author Fred Campos/CIS2232
 * @since 2021-10-14
 * @modified 20211030 fhm Modified for student use.
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

}
