package info.hccis.tutoring.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DAO class for Tutor Skill report
 * @author Carter Phillips
 * @since 20211001
 */
public class TutorDAO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;

    public TutorDAO() {

        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        String connectionString = rb.getString("spring.datasource.url");
        String userName = rb.getString("spring.datasource.username");
        String password = rb.getString("spring.datasource.password");

        //String URL = "jdbc:mysql://" + "localhost" + ":8081/" + "cis2232_tutor";
        try {
            conn = DriverManager.getConnection(connectionString, userName, password);
        } catch (Exception e) {
            System.out.println("Error");
        }


    }

    /**
     * Select a TutorSkill by name
     *
     * @since 20210924
     * @author Carter Phillips
     */
    public ArrayList<String> selectTutorInformationByTutorSkill(String tutorSkill) {
        Statement stmt = null;
        ArrayList<String> names = new ArrayList();
        try {
            stmt = conn.createStatement();
            
              
            String query = "SELECT s.firstName, s.lastName, s.phoneNumber, s.emailAddress "
            + "FROM Tutor s WHERE s.id in ("
            + "SELECT sc.tutorId FROM tutorSkill sc WHERE sc.skillType = "
            + "(SELECT cv.codeValueSequence FROM CodeValue cv "
            + "where cv.englishDescription = '" + tutorSkill
            + "' and cv.codeTypeId = 3))";
                    
            
            
            System.out.println(query);
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

        try {
            while (rs.next()) {
                
                names.add(rs.getString(1) + ", " + rs.getString(2) + ", " + rs.getString(3) + ", " + rs.getString(4));
                System.out.println("Found a Tutor: "+rs.getString(1));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(TutorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return names;
    }

}
