package info.hccis.tutoring.dao;

import info.hccis.tutoring.entity.TutorCodeValue;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DAO class for tutor skill report
 *
 * @author Carter Phillips
 * @since 20211001
 */
public class TutorCodeValueDAO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;

    public TutorCodeValueDAO() {

        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        String connectionString = rb.getString("spring.datasource.url");
        String userName = rb.getString("spring.datasource.username");
        String password = rb.getString("spring.datasource.password");

        //String URL = "jdbc:mysql://" + "localhost" + ":8081/" + "cis2232_tutor";
        try {
            conn = DriverManager.getConnection(connectionString, userName, password);
        } catch (Exception e) {
            System.out.println("Error");
        }

    }

    /**
     * Select all code values based on code type
     *
     * @since 20210924
     * @author Carter Phillips
     */
    public ArrayList<TutorCodeValue> selectCodeValues(int codeTypeId) {
        Statement stmt = null;
        ArrayList<TutorCodeValue> codes = new ArrayList();
        try {
            stmt = conn.createStatement();
            String query = "SELECT * FROM CodeValue cv WHERE cv.codeTypeId = "+codeTypeId+" ORDER BY cv.englishDescription;";
            System.out.println(query);
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

        try {
            while (rs.next()) {
                codes.add(new TutorCodeValue(rs.getInt("codeTypeId"), rs.getInt(2), rs.getString(3)));
                System.out.println("Found a code value: "+rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TutorCodeValueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return codes;
    }

}
