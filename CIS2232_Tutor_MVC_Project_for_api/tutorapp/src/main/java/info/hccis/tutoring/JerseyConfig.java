package info.hccis.tutoring;

import info.hccis.tutoring.rest.InvoiceService;
import info.hccis.tutoring.rest.StudentService;
import info.hccis.tutoring.rest.TutorService;
import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * @author N/A
 * @since N/A
 * @modified 20211122 fhm Added StudentService.class.
 */
@Component
@ApplicationPath("api")
public class JerseyConfig extends ResourceConfig {

	@PostConstruct
	private void init() {
		registerClasses(InvoiceService.class);
		registerClasses(TutorService.class);
		registerClasses(StudentService.class);
	}
}
