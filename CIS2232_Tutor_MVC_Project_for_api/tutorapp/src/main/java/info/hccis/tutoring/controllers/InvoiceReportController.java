package info.hccis.tutoring.controllers;

import info.hccis.tutoring.bo.InvoiceBO;
import info.hccis.tutoring.entity.InvoiceReport;
import info.hccis.tutoring.repositories.TutorRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller handles all invoice reports request mappings
 *
 * @author Alonso Briones
 */
@Controller
@RequestMapping("/reports")
public class InvoiceReportController {

    private final TutorRepository _tr;

    /**
     * Create Tutor repository
     *
     * @param ir
     * @param tr
     */
    @Autowired
    public InvoiceReportController(TutorRepository tr) {
        _tr = tr;
    }

    /**
     * General Mapping
     * 
     * @return reports
     */
    @RequestMapping("/reports")
    public String reports() {
        return "reports/reports";
    }

    /**
     * Mapping for the invoice report
     * 
     * @param model
     * @return 
     */
    @RequestMapping("/invoiceReport")
    public String reports1(Model model) {
        model.addAttribute("invoiceReport", new InvoiceReport());
        model.addAttribute("tutors", _tr.findAll());
        return "reports/invoiceReport";
    }

    /**
     * Mapping for submit on invoiceReport
     * 
     * @param model
     * @param invoiceReport
     * @return invoiceReport
     */
    @RequestMapping("/submitInvoiceReport")
    public String submitInvoiceReport(Model model, @ModelAttribute("invoiceReport") InvoiceReport invoiceReport) {
        System.out.println("Running invoice report...");
        //Invoke model business logic which will access the database
        InvoiceBO ibo = new InvoiceBO();
        ArrayList<InvoiceReport> invoiceReports = ibo.showInvoiceByName(invoiceReport.getTutorFullName());

        invoiceReport.setInvoiceReport(invoiceReports);
        model.addAttribute("invoiceReport", invoiceReport);
        model.addAttribute("tutors", _tr.findAll());
        return "reports/invoiceReport";
    }
}
