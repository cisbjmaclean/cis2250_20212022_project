package info.hccis.tutoring.entity;

import java.util.ArrayList;

/**
 * This class gathers all the information needed from the Tutor and Invoice classes
 * to create a invoice report
 * @author Alonso Briones
 */
public class InvoiceReport {

    private int id;
    private int numHours;
    private String engDescription;
    private String startDate;
    private String endDate;
    private String tutorFirstName;
    private String tutorLastName;
    private String tutorFullName;

    public String getTutorFullName() {
        return tutorFullName;
    }

    public void setTutorFullName(String tutorFullName) {
        this.tutorFullName = tutorFullName;
    }

    private ArrayList<InvoiceReport> invoiceReport = new ArrayList();
    
    public InvoiceReport(){
        this.tutorFirstName = "";
        this.tutorLastName = "";
    }

    public void setInvoiceReport(ArrayList<InvoiceReport> invoiceReport) {
        this.invoiceReport = invoiceReport;
    }

    public ArrayList<InvoiceReport> getInvoiceReport() {
        return invoiceReport;
    }

    public String getTutorFirstName() {
        return tutorFirstName;
    }

    public void setTutorFirstName(String tutorFirstName) {
        this.tutorFirstName = tutorFirstName;
    }

    public String getTutorLastName() {
        return tutorLastName;
    }

    public void setTutorLastName(String tutorLastName) {
        this.tutorLastName = tutorLastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumHours() {
        return numHours;
    }

    public void setNumHours(int numHours) {
        this.numHours = numHours;
    }

    public String getEngDescription() {
        return engDescription;
    }

    public void setEngDescription(String engDescription) {
        this.engDescription = engDescription;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String toString() {
        String output = id + "\t" + numHours + "\t" + engDescription + "\t" + startDate + "\t" + endDate;
        return output;
    }

}
