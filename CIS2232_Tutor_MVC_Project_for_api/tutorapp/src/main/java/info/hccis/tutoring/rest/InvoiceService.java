package info.hccis.tutoring.rest;
import com.google.gson.Gson;
import info.hccis.tutoring.entity.Invoice;
import info.hccis.tutoring.repositories.InvoiceRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Invoice Service
 * 
 * @author Alonso Briones
 */
@Path("/InvoiceService/invoice")
public class InvoiceService {
    
    private final InvoiceRepository _ir;

    @Autowired
    public InvoiceService(InvoiceRepository _ir) {
        this._ir = _ir;
    }
    @GET
    @Produces("application/json")
    public ArrayList<Invoice> getAll() {
        ArrayList<Invoice> invoices = (ArrayList<Invoice>) _ir.findAll();
        return invoices;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getInvoiceById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Invoice> invoice = _ir.findById(id);

        if (!invoice.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(invoice).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createInvoice(String invoiceJson) 
    {        
        try{
            String temp = save(invoiceJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
    
    @DELETE
    @Path("/{id}")
    public Response deleteInvoice(@PathParam("id") int id) throws URISyntaxException {
        Optional<Invoice> invoice = _ir.findById(id);
        if(invoice != null) {
            _ir.deleteById(id);
            return Response.status(HttpURLConnection.HTTP_CREATED).build();
        }
        return Response.status(404).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateInvoice(@PathParam("id") int id, String invoiceJson) throws URISyntaxException 
    {
        try{
            String temp = save(invoiceJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }

    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        Invoice invoice = gson.fromJson(json, Invoice.class);
 
        if(invoice.getId() == null){
            invoice.setId(0);
        }

        invoice = _ir.save(invoice);

        String temp = "";
        temp = gson.toJson(invoice);

        return temp;
    }
}
