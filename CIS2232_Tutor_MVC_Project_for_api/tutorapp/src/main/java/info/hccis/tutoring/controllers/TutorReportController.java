package info.hccis.tutoring.controllers;



import info.hccis.tutoring.bo.TutorCodeValueBO;
import info.hccis.tutoring.entity.TutorCodeValue;

import info.hccis.tutoring.bo.TutorBO;
import info.hccis.tutoring.entity.TutorDetail;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


//Note courses = tutors & course = tutor



@Controller
@RequestMapping("/reports")
public class TutorReportController {

    @RequestMapping("/tutorReport")
    public String about(Model model, HttpSession session) {
        System.out.println("this action will send to the Tutor Skill report input view");
        model.addAttribute("tutorDetail", new TutorDetail());
        
        //Check to see if the tutorSkill collection is in the session
        ArrayList<TutorCodeValue> courses = (ArrayList<TutorCodeValue>) session.getAttribute("courses");
        if (courses == null || courses.isEmpty()) {
            TutorCodeValueBO codeValueBO = new TutorCodeValueBO();
            session.setAttribute("courses", codeValueBO.selectCodeValues(3));  //tutorSkill == 3
        }
        return "reports/tutorReport";
    }

    @RequestMapping("/tutorSkillReportSubmit")
    public String submitStudentCourseReport(Model model, @ModelAttribute("tutorDetail") TutorDetail tutorDetail) {
        System.out.println("submitting to run the report: " + tutorDetail.getTutorSkill());

        //Invoke model business logic which will access the database
        TutorBO rscbo = new TutorBO();
        ArrayList<String> names = rscbo.getTutorInformationForTutorSkill(tutorDetail.getTutorSkill());

        tutorDetail.setNames(names);
        tutorDetail.setTutorSkillForTutorInformationFound(tutorDetail.getTutorSkill());
        tutorDetail.setTutorSkill("");

        if (names.isEmpty()) {
            model.addAttribute("message", "No tutor found");
        } else {
            model.addAttribute("message", "Tutor report loaded");
        }

        model.addAttribute("tutorDetail", tutorDetail);

        return "reports/tutorReport";
    }

}
