package info.hccis.tutoring.entity;

/**
 * Class used to represent a course.
 *
 * @author fhm
 * @since 20211018
 */
public class Code {

	private int id;
	private String name;

	public Code(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
