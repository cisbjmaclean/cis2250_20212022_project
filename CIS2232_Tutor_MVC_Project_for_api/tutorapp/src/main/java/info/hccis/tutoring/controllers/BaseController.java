package info.hccis.tutoring.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {
    
    @RequestMapping("/")
    public String index() {
	return "index";
    }

    @RequestMapping("/home")
    public String home() {
	return "home/home";
    }

    @RequestMapping("/apis")
    public String api() {
	return "api/apis";
    }

    @RequestMapping("/other")
    public String about() {
	return "other/other";
    }

    @RequestMapping("/other/about_cp")
    public String about1() {
	return "other/about_cp";
    }

    @RequestMapping("/other/about_fb")
    public String about2() {
	return "other/about_fb";
    }

    @RequestMapping("/other/about_fhm")
    public String about3() {
	return "other/about_fhm";
    }

    @RequestMapping("/other/help")
    public String help() {
	return "other/help";
    }

    @RequestMapping("/future")
    public String future() {
	return "fragments/future";
    }

}
