package info.hccis.tutoring.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Class used to represent a student.
 *
 * @author fhm
 * @since 20211018
 * @modified 20211030 fhm Now supports repository.
 */
@Entity
@Table(name = "Student")
public class Student implements Serializable {

	//The names of the variables might not match what is in the DB. This is for backwards compatibility (report).
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private int id;
	@Size(max = 40)
	@Column(name = "firstName")
	private String firstName;
	@Size(max = 40)
	@Column(name = "lastName")
	private String lastName;
	@Size(max = 10)
	@Column(name = "phoneNumber")
	private String phoneNumber;
	@Size(max = 100)
	@Column(name = "emailAddress")
	private String emailAddress;
	@Column(name = "programCode")
	private Integer programCode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Integer getProgramCode() {
        return programCode;
    }

    public void setProgramCode(Integer programCode) {
        this.programCode = programCode;
    }

}
