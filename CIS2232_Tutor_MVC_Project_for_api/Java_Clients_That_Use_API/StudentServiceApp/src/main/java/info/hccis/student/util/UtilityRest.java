package info.hccis.student.util;

import com.google.gson.Gson;
import info.hccis.student.model.jpa.Student;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author bjmaclean
 * @since Nov 17, 2017
 * @modified 20211122 fhm Remove redundant code, set correct HTTP methods,
 * uncomment and fix provided code. Also purposely prove that individual ID
 * selection works. Also removed most comments and or modified them.
 */
public class UtilityRest {

	/**
	 *
	 * @since 20171117
	 * @author BJM
	 * @modified 2021122 fhm
	 */
	public static String getJsonFromRest(String urlString) {
		String content = "";
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() == 204) {
				return null;
			} else if (conn.getResponseCode() != 200) {
				throw new RuntimeException("\nFailed : HTTP error code : " + conn.getResponseCode() + "\n" + conn.getResponseMessage());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while (br.ready()) {
				content += br.readLine();
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 *
	 * @since 20171117
	 * @author BJM
	 * @modified 2021122 fhm
	 */
	public static String getJsonFromRestById(String urlString, int id) {
		String content = "";
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(urlString + id).openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() == 204) {
				return null;
			} else if (conn.getResponseCode() != 200) {
				System.out.println("\nFailed : HTTP error code : " + conn.getResponseCode() + "\n" + conn.getResponseMessage());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while (br.ready()) {
				content += br.readLine();
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//To fool this into thinking it's an array.
		return "[" + content + "]";
	}

	/**
	 *
	 * @since 20161115
	 * @author BJM
	 * @modified 2021122 fhm
	 */
	public static void addUsingRest(String urlIn, Object objectIn) {
		Gson gson = new Gson();

		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(urlIn).openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

			OutputStream os = conn.getOutputStream();
			os.write(gson.toJson(objectIn).getBytes());
			os.flush();

			if (conn.getResponseCode() != 201) {
				System.out.println("\nFailed : HTTP error code : " + conn.getResponseCode() + "\n" + conn.getResponseMessage());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String content = "";
				while (br.ready()) {
					content += br.readLine();
				}
				Student studentReturned = gson.fromJson(content, Student.class);
				System.out.println("\nSuccess : Added student (" + studentReturned.getId() + ").");
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 *
	 * @since 20161115
	 * @author BJM
	 * @modified 2021122 fhm
	 */
	public static void updateUsingRest(String urlIn, Student student) {
		Gson gson = new Gson();

		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(urlIn).openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

			OutputStream os = conn.getOutputStream();
			os.write(gson.toJson(student).getBytes());
			os.flush();

			if(conn.getResponseCode() == 204){
				System.out.println("\nNo data found.");
			} else if (conn.getResponseCode() != 200) {
				System.out.println("\nFailed : HTTP error code : " + conn.getResponseCode() + "\n" + conn.getResponseMessage());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String content = "";
				while (br.ready()) {
					content += br.readLine();
				}
				System.out.println("\nSuccess : Updated student (" + gson.fromJson(content, Student.class).getId() + ").");
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method will access the booking service to delete a booking
	 *
	 * @since 20200618
	 * @author BJM
	 */
	public static void deleteUsingRest(String urlIn, int id) {
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(urlIn + id).openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("DELETE");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			//conn.getOutputStream();

			if(conn.getResponseCode() == 204){
				System.out.println("\nNo data found.");
			} else if (conn.getResponseCode() != 200) {
				System.out.println("\nFailed : HTTP error code : " + conn.getResponseCode() + "\n" + conn.getResponseMessage());
			} else {
				System.out.println("\nSuccess : deleted student (" + id + ").");
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
