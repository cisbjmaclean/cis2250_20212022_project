package info.hccis.student.main;

import com.google.gson.Gson;
import info.hccis.student.model.jpa.Student;
import info.hccis.student.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;

/**
 *
 * @author bjmaclean
 * @since N/A
 * @modified 20211122 fhm Uncomment and fix provided code, remove redundant
 * code, implement individual ID selection. Removed/updated comments.
 */
public class Controller {

	final public static String MENU = "\nMain Menu\n(A)dd\n(U)pdate\n(D)elete\n(V)iew\n(Q)uit";
	private static final Scanner input = new Scanner(System.in);
	private static final String URL_STRING = "http://localhost:8081/api/StudentService/student/";

	public static void main(String[] args) {
		boolean endProgram = false;
		do {
			System.out.println(MENU);
			Student student;
			switch (input.nextLine().toUpperCase()) {
				case "A":
					student = create(false);
					UtilityRest.addUsingRest(URL_STRING, student);
					break;
				case "U":
					student = create(true);
					UtilityRest.updateUsingRest(URL_STRING, student);
					break;
				case "D":
					System.out.println("ID to delete:");
					int id = input.nextInt();
					input.nextLine();//burn
					UtilityRest.deleteUsingRest(URL_STRING, id);
					break;
				case "V":
					System.out.println("(I)D or (A)ll?");
					//Needed to avoid null error.
					String content;
					if (input.nextLine().toLowerCase().equals("i")) {
						System.out.println("Student ID:");
						content = UtilityRest.getJsonFromRestById(URL_STRING, input.nextInt());
						input.nextLine();//burn
					} else {
						content = UtilityRest.getJsonFromRest(URL_STRING);
					}
					System.out.println();
					if(content == null){
						System.out.println("No data found.");
						break;
					}					
					JSONArray jsonArray = new JSONArray(content);
					Gson gson = new Gson();
					for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
						Student current = gson.fromJson(jsonArray.getJSONObject(currentIndex).toString(), Student.class);
						System.out.println("Student ID: " + current.getId()
								+ "\nFirst Name: " + current.getfName()
								+ "\nLast Name: " + current.getlName()
								+ "\ne-Mail: " + current.geteMail()
								+ "\nPhone number: " + current.getpNum()
								+ "\nProgram ID: " + current.getProg()
								+ "\n");
					}
					break;
				case "Q":
					endProgram = true;
					break;
				default:
					System.out.println("Invalid choice. Try again.");
			}
		} while (!endProgram);
	}

	/**
	 * Create an object by passing asking user for input.
	 *
	 * @param askId on false does not ask for the id.
	 * @return object
	 * @since 20171117
	 * @author BJM
	 * @modified 20211122 fhm Now has a variable to avoid asking the ID, and has
	 * been adapted to the updated student class.
	 */
	public static Student create(boolean askId) {
		Student student = new Student();
		if (askId) {
			System.out.println("Enter id:");
			student.setId(Integer.parseInt(input.nextLine()));
		}
		System.out.println("First Name:");
		student.setfName(input.nextLine());
		System.out.println("Last Name:");
		student.setlName(input.nextLine());
		System.out.println("e-Mail:");
		student.seteMail(input.nextLine());
		System.out.println("Phone number:");
		student.setpNum(input.nextLine());
		System.out.println("Program:");
		student.setProg(Integer.parseInt(input.nextLine()));
		return student;
	}

}
