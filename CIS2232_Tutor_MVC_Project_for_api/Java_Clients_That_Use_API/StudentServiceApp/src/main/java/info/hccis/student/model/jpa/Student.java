package info.hccis.student.model.jpa;

import java.io.Serializable;

/**
 * @author bjmaclean
 * @since Oct 28, 2021
 * @modified 20211122 fhm Copy student from TutorApp and adjust to fit the
 * needs.
 */
public class Student implements Serializable {

	private int id, prog;
	private String fName, lName, pNum, eMail;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getpNum() {
		return pNum;
	}

	public void setpNum(String pNum) {
		this.pNum = pNum;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Integer getProg() {
		return prog;
	}

	public void setProg(Integer prog) {
		this.prog = prog;
	}
}
