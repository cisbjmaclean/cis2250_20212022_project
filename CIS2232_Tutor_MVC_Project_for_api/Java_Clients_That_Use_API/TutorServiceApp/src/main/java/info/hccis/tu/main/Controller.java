package info.hccis.tu.main;

import com.google.gson.Gson;
import info.hccis.tu.model.jpa.Tutor;
import info.hccis.tu.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;

public class Controller {

    final public static String MENU = "\nMain Menu \nA) Add\n"
            + "U) Update \n"
            + "V) View\n"
            + "F) View One\n"
            + "D) Delete \n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    private static final String URL_STRING = "http://localhost:8081/api/TutorService/tutor/";

    public static void main(String[] args) {
        boolean endProgram = false;
        do {
            System.out.println(MENU);
            String choice = input.nextLine();
            Tutor tutor;
            String url;
            switch (choice.toUpperCase()) {
                case "A":
                    tutor = create();
                    url = URL_STRING;
                    System.out.println("Url=" + url);
                    UtilityRest.addUsingRest(url, tutor);
                    break;
                case "U":
                    //camper = createCamper();
                    //url = URL_STRING;
                    //System.out.println("Url="+url);
                    //UtilityRest.addUsingRest(url, camper);
                    break;
                case "D":
                    System.out.println("Enter id to delete");
                    Scanner input = new Scanner(System.in);
                    int id = input.nextInt();
                    input.nextLine();  //burn
                    UtilityRest.deleteUsingRest(URL_STRING, id);
                    break;
                case "V":
                    String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);
                    //**************************************************************
                    //Based on the json string passed back, loop through each json
                    //object which is a json string in an array of json strings.
                    //*************************************************************
                    JSONArray jsonArray = new JSONArray(jsonReturned);
                    //**************************************************************
                    //For each json object in the array, show the first and last names
                    //**************************************************************
                    System.out.println("Here are the rows");
                    Gson gson = new Gson();
                    for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
                        Tutor current = gson.fromJson(jsonArray.getJSONObject(currentIndex).toString(), Tutor.class);
                        System.out.println(current.toString());
                    }
                    break;
                case "F":
                    
                    
                    /*
                    System.out.println("Enter id to Enter");
                    Scanner input = new Scanner(System.in);
                    url = URL_STRING;
                    System.out.println("Url=" + url);
                    UtilityRest.addUsingRest(url, tutor);
                    
                    System.out.println("Enter id to Enter");
                    Scanner input = new Scanner(System.in);
                    //int id = input.nextInt();
                    input.nextLine();  //burn
                    UtilityRest.getSingleJsonFromRest(URL_STRING, id);
                    */
                    
                    
                    
                    System.out.println("Enter id to Enter");
                    Scanner nput = new Scanner(System.in);
                    id = nput.nextInt();
                    nput.nextLine();
                    UtilityRest.getSingleJsonFromRest(URL_STRING, id);
                    System.out.println(URL_STRING + id);
                    
                    /*
                    //**************************************************************
                    //Based on the json string passed back, loop through each json
                    //object which is a json string in an array of json strings.
                    //*************************************************************
                    //JSONArray jsonArray = new JSONArray(jsonReturned);
                    //**************************************************************
                    //For each json object in the array, show the first and last names
                    //**************************************************************
                    System.out.println("Here are the rows");
                    Gson gson = new Gson();
                    for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
                        Tutor current = gson.fromJson(jsonArray.getJSONObject(currentIndex).toString(), Tutor.class);
                        System.out.println(current.toString());
                    }
                    */
                    
                    
                    
                    
                    
                    break;
                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        } while (!endProgram);
    }

    /**
     * Create an object by passing asking user for input.
     *
     * @return object
     * @since Nov 19, 2021
     * @author Carter Phillips
     */
    public static Tutor create() {
        Tutor tutor = new Tutor();
        System.out.println("Enter id: (0 for add)");
        tutor.setId(Integer.parseInt(input.nextLine()));

        System.out.println("First Name:");
        tutor.setFirstName(input.nextLine());

        System.out.println("Last Name:");
        tutor.setLastName(input.nextLine());
        
        System.out.println("Phone Number:");
        tutor.setPhoneNumber(input.nextLine());
        
        System.out.println("Email Address:");
        tutor.setEmailAddress(input.nextLine());
        
        System.out.println("Address:");
        tutor.setAddress(input.nextLine());
        
        System.out.println("Employee Type:");
        tutor.setEmployeeType(Integer.parseInt(input.nextLine()));

        return tutor;
    }

}
