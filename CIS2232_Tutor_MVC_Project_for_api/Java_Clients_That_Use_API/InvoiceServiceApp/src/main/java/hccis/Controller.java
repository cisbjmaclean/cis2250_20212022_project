package hccis;

import hccis.bo.Invoice;
import hccis.util.CisUtility;
import hccis.util.UtilityRest;
import com.google.gson.Gson;
import org.json.*;

/**
 * This project access the invoice service in the TutorApp to add, uptade, and
 * delete invoices
 *
 * @author Alonso briones
 * @since 11/15/2021
 */
public class Controller {

    final public static String MENU = "\nMain Menu \n"
            + "A) Add\n"
            + "U) Update\n"
            + "V) View\n"
            + "D) Delete\n"
            + "X) Exit";
    private static final String URL_STRING = "http://localhost:8081/api/InvoiceService/invoice/";

    public static void main(String[] args) {
        Boolean exit = false;
        do {
            String option = CisUtility.getInputString(MENU);
            String url;
            String json;
            String output = "";

            Invoice invoice;
            int invoiceId;

            Gson gson = new Gson();
            JSONObject jsonObject;

            switch (option.toUpperCase()) {
                case "A":
                    invoice = createInvoice(); //Create invoice object
                    url = URL_STRING;
                    System.out.println("Url=" + url);
                    UtilityRest.addUsingRest(url, invoice);
                    break;
                case "U":
                    invoiceId = CisUtility.getInputInt("Enter invoice ID to update:");
                    url = URL_STRING + invoiceId;

                    if (verifyIdExist(invoiceId)) { //Verify that id exixts
                        json = UtilityRest.getJsonFromRest(url);
                        jsonObject = new JSONObject(json);
                        invoice = gson.fromJson(json, Invoice.class);

                        invoice.setTutorId(CisUtility.getInputInt("Enter tutor id:"));
                        invoice.setStartDate(CisUtility.getInputString("Enter start date:"));
                        invoice.setEndDate(CisUtility.getInputString("Enter end date:"));

                        UtilityRest.updateUsingRest(url, invoice);
                    } else {
                        System.out.println("Id entered does not exist");
                    }

                    break;
                case "V":
                    String viewMenu = "(1) View All\n"
                            + "(2) View by ID";
                    int viewOption = CisUtility.getInputInt(viewMenu);

                    switch (viewOption) {
                        case 1:
                            url = URL_STRING;
                            json = UtilityRest.getJsonFromRest(url);
                            JSONArray invoices = new JSONArray(json);
                            for (int i = 0; i < invoices.length(); i++) {
                                JSONObject invoiceReturned = invoices.getJSONObject(i); //Get object from JSONArrayList

                                output += "Invoice ID: " + invoiceReturned.get("id")
                                        + ", Tutor ID: " + invoiceReturned.get("tutorId")
                                        + ", Start Date: " + invoiceReturned.get("startDate")
                                        + ", End Date: " + invoiceReturned.get("endDate") + "\n";
                            }
                            System.out.println(output);
                            break;
                        case 2:
                            invoiceId = CisUtility.getInputInt("Enter invoice ID:");
                            url = URL_STRING + invoiceId;

                            if (verifyIdExist(invoiceId)) { //Verify that id exists
                                json = UtilityRest.getJsonFromRest(url);
                                jsonObject = new JSONObject(json);

                                output = "Invoice ID: " + jsonObject.get("id")
                                        + ", Tutor ID: " + jsonObject.get("tutorId")
                                        + ", Start Date: " + jsonObject.get("startDate")
                                        + ", End Date: " + jsonObject.get("endDate");
                                System.out.println(output);
                            } else {
                                System.out.println("Id entered does not exist");
                            }

                            break;
                        default:
                            System.out.println("Invalid Input");
                            break;
                    }
                    break;
                case "D":
                    invoiceId = CisUtility.getInputInt("Enter invoice ID to delete:");
                    url = URL_STRING + invoiceId;

                    json = UtilityRest.getJsonFromRest(url);
                    jsonObject = new JSONObject(json);
                    UtilityRest.deleteUsingRest(URL_STRING, Integer.parseInt(jsonObject.get("id").toString()));
                    break;
                case "X":
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid input");
                    break;

            }
        } while (!exit);
    }

    /**
     * Create an object by passing asking user for input.
     *
     * @return object
     * @since 20211115
     * @author Alonso Briones
     */
    public static Invoice createInvoice() {
        Invoice invoice = new Invoice();
        invoice.setTutorId(CisUtility.getInputInt("Enter tutor id:"));
        invoice.setStartDate(CisUtility.getInputString("Enter start date:"));
        invoice.setEndDate(CisUtility.getInputString("Enter end date:"));

        return invoice;
    }

    /**
     * Loop through all the invoice IDs to verify that the ID passed as a
     * parameter exists
     *
     * @param id InvoiceId
     * @return Boolean
     * @author Alonso Briones
     * @since 11/18/2021
     */
    public static boolean verifyIdExist(int id) {
        String json = UtilityRest.getJsonFromRest(URL_STRING);
        JSONArray invoices = new JSONArray(json);
        for (int i = 0; i < invoices.length(); i++) {
            JSONObject invoiceReturned = invoices.getJSONObject(i);

            if ((int) invoiceReturned.get("id") == id) {
                return true;
            }
        }
        return false;
    }

}
